\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{The Recommender Problem}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Motivations}{7}{0}{1}
\beamer@subsectionintoc {1}{3}{Our strategy}{10}{0}{1}
\beamer@sectionintoc {2}{State of the Art}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Functions}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Common Techniques}{13}{0}{2}
\beamer@subsectionintoc {2}{3}{Challenges}{14}{0}{2}
\beamer@sectionintoc {3}{System Overview: a Hybrid Recommender System}{15}{0}{3}
\beamer@subsectionintoc {3}{1}{Problem}{16}{0}{3}
\beamer@subsectionintoc {3}{2}{Design and Methodology}{17}{0}{3}
\beamer@sectionintoc {4}{Results}{25}{0}{4}
\beamer@subsectionintoc {4}{1}{Dataset}{26}{0}{4}
\beamer@subsectionintoc {4}{2}{Evaluation}{27}{0}{4}
\beamer@sectionintoc {5}{Conclusions and Future Works}{31}{0}{5}
