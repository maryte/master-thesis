----------------------
Command Line Arguments
----------------------

Example:  run.sh

----------------------
Abstract
----------------------

Recommender Systems are software techniques providing suggestions for items to be of use to a user. They have changed the way people find products, information, and even other people. The work I am going to present in this thesis is based on the research I did during the Internship at the Knowledge Engineering & Machine Learning Group · BarcelonaTech. Our aim was the analysis and comparison of the software tools involved in the intelligent Recommender Systems field to get the knowledge for the design of a novel hybrid approach result of the combination of Collaborative Filtering and Association Rules techniques. We also investigate the Diversification aspect, taking into account the propensity for novelty of the single user. The concrete movies domain was selected to illustrate the application of the undertaken research because of the relevance it occupies nowadays in the e-commerce environment. A test evaluation of the several strategies has been provided to outline the performances of the explored methodologies.