#!/usr/bin/env python
# collaborative filtering matrix

from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt
from pcc_calculation import pcc
	
def main():

	
	np.seterr(all='raise')
	
	if len (sys.argv) != 3:
		print ("Usage: <output file> <n_users>")
		sys.exit (1)
		
	output_file = sys.argv[1]
	NUSERS = int(sys.argv[2])
	
	# users_mx = [[None for x in range(NUSERS)] for x in range(NUSERS)] 
	
	# for user in range(1,NUSERS):
		# for other in range(1,NUSERS):
			# users_mx[user][other] = pcc(str(user),str(other))[0]
	
	#users_mx = np.array([[pcc(str(user), str(other))[0] for user in range(NUSERS)] for other in range(NUSERS)])
	
	#print (users_mx)
	
	wb = xlwt.Workbook()
	ws = wb.add_sheet('users_mx')

	for user in range(NUSERS):
		for other in range(NUSERS):
			ws.write(user, other, pcc(str(user), str(other))[0])

	wb.save(output_file)
	
	 

if __name__ == "__main__":
    main()