#!/usr/bin/env python
from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt
import sqlite3
import glob
import xml.etree.ElementTree as ET
import omdb
import untangle
import random
import operator
from sklearn.metrics import mean_squared_error
import parameters

def pad_imdb_id(imdb):
	if len(str(imdb)) == 9:
		return imdb
	while (len(str(imdb)) < 7):
		imdb = "0" + imdb
	return "tt" + imdb

def percentage(whole):
  return int((parameters.PERCENTAGE/100)*float(whole))
				
def evaluation(predicted,removed,scores):

	#print ("\n\nEVALUATION:")
	# print ("\nPredicted: "+str(len(predicted)))
	
	predicted_items = {}
	unpredicted_items = {}
	predicted_len = 0
	precision = 0
	recall = 0
	lift = 0 
	norm_rmse = 0
	true_n_users = 0
	
	y_true = []
	y_pred = []
	high_rank_removed_items = {}
	high_rank_predicted_items = {}

	for imdb_id in removed.keys():		
		
		if float(removed[imdb_id]) > parameters.MIN_RANK:
			high_rank_removed_items[imdb_id] = removed[imdb_id]
			
		if imdb_id in predicted.keys():		
			if (float(removed[imdb_id]) > parameters.MIN_RANK):
				high_rank_predicted_items[imdb_id] = scores[imdb_id]
					
			predicted_items[imdb_id]=scores[imdb_id]
						
			y_true.append(float(removed[imdb_id]))
			y_pred.append(float(predicted_items[imdb_id]))
			#print ("recall yes")

						
		else:
			unpredicted_items[imdb_id]=removed[imdb_id]
			

	################## PREDICTED LEN	
	predicted_len = len(predicted)
	
	################## PRECISION	
	try:
		precision = (len(predicted_items)*100)/predicted_len
	except ZeroDivisionError as e:
		precision = 0
	
	################## RECALL		
	try:
		recall = (len(predicted_items)*100)/len(removed)
	except ZeroDivisionError as e:
		recall = 0
	#print ("recall: ",len(predicted_items), "/",len(removed)," :" ,int(recall),"%") 
	## print ("Predicted scores ",str(predicted_items))	
	
	################## LIFT
	try:
		lift = (len(high_rank_predicted_items)*100)/len(high_rank_removed_items)
	except ZeroDivisionError as e:
		lift = 0
	# print ("LIFT > MIN_RANK: ",len(high_rank_predicted_items), "/",len(high_rank_removed_items)," :" ,int(lift),"%")
	
	################## RMSE
	if len(y_pred) > 0:
		rmse = sqrt(mean_squared_error(y_true, y_pred))
		# print ("RMSE:", rmse)
		norm_rmse = round(((rmse/5)*100),2)
		# print ("normalized RMSE:",norm_rmse,"%")

	if len(y_pred) > 0:
		true_n_users = 1
		
	return predicted_len,precision,recall,lift,norm_rmse,true_n_users


def ap_evaluation(removed,predicted):

	# print ("\n\nEVALUATION:")
	# print ("\nPredicted: "+str(len(predicted)))
	
	predicted_items = {}
	unpredicted_items = {}
	true_n_users = 0
	
	high_rank_removed_items = {}

	for imdb_id in removed.keys():		
		
		if imdb_id in predicted.keys():
			if float(removed[imdb_id]) > parameters.MIN_RANK:
				high_rank_removed_items[imdb_id] = removed[imdb_id]		
			predicted_items[imdb_id]=removed[imdb_id]						
		
		else:
			unpredicted_items[imdb_id]=removed[imdb_id]
			

	################## PREDICTED LEN	
	predicted_len = len(predicted)
	
	################## PRECISION		
	try:
		precision = (len(predicted_items)*100)/predicted_len
	except ZeroDivisionError as e:
		precision = 0
	
	################## RECALL		
	try:
		recall = (len(predicted_items)*100)/len(removed)
	except ZeroDivisionError as e:
		recall = 0
	# print ("recall: ",len(predicted_items), "/",len(removed)," :" ,int(recall),"%") 
	## print ("Predicted scores ",str(predicted_items))	
	
	################## LIFT
	try:
		lift = (len(high_rank_removed_items)*100)/len(removed)
	except ZeroDivisionError as e:
		lift = 0
	# print ("LIFT > MIN_RANK: ",len(high_rank_predicted_items), "/",len(high_rank_removed_items)," :" ,int(lift),"%")
	
	if len(predicted_items) > 0:
		true_n_users = 1
	
	return predicted_len,precision,recall,lift, true_n_users