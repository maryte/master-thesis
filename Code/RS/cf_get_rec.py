#!/usr/bin/env python
from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt
import sqlite3
import glob
import xml.etree.ElementTree as ET
import omdb
import untangle
import random
import operator
from sklearn.metrics import mean_squared_error
from rec_evaluation import evaluation
import parameters

def get_closer_users(person,input_file):

	book = xlrd.open_workbook(input_file)
	person_sh = book.sheet_by_index(0)

	#extract more similar ppcs
	ppcs = person_sh.row_values(int(person))
	#print ("user ppcs: ",user_ppcs)
	
	user_ppcs = {}
	for u in range(len(ppcs)):
		if ppcs[u] != "":
			user_ppcs[u]=float(ppcs[u])
		else:
			user_ppcs[u]= (-1)
		
	return sorted(user_ppcs.items(), key=lambda x:x[1], reverse = True)
	
				
def cf_rec(person,my_ratings,input_file):

	sorted_user_ppcs = get_closer_users(person,input_file)
	
	# Gets recommendations for a person by using a weighted average of every other user's predicted
	totals = {}
	n_ratings = {}
	simSums = {}

	# Create a SQLite database for the results.
	conn = sqlite3.connect(parameters.DB)

	other_ratings = {}

	#print ("MY_rat of user "+str(person)+" after "+ str(PERCENTAGE) +"% pop: "+str(len(my_ratings)))

	#only firsts closer users
	for other in range(parameters.CLOSER_PPC):
	#for other in range(len(sorted_user_ppcs)):
		#print sorted_user_ppcs[other]
		# ignore scores of zero or lower or with myself
		if (sorted_user_ppcs[other][0] == person): 
			#print ("continue")
			continue
		#query to the internal sqlite db
		other_ratings = {}
		other_rat = conn.execute("SELECT imdb_id,rating from reviews where userId =" + str(sorted_user_ppcs[other][0]))
		
		for row in other_rat:
			other_ratings[str(row[0])] = str(row[1])
			# print "similar user: ", other
		 		
		for movieId in other_ratings:
			#if not seen yet
			if movieId not in my_ratings:
				# Similarity * score
				#print ("not seen movie: ", row)
				totals.setdefault(movieId,0)
				n_ratings.setdefault(movieId,0)
				#print (totals[movieId])
				#print (other_ratings[str(movieId)])
				#print (sorted_user_ppcs[other])
				totals[movieId] += (float(other_ratings[str(movieId)]) * float(str(sorted_user_ppcs[other][1]))) #score * weight
				n_ratings[movieId] = n_ratings[movieId] +1
				# sum of similarities
				simSums.setdefault(movieId,0)
				simSums[movieId]+= float(sorted_user_ppcs[other][1]) #weights

	predicted = {}
	
	# Create the normalized list
	for item,total in totals.items():
		try:
			rank = total/simSums[item]
		except ZeroDivisionError as e:
			rank = 0
		predicted[item] = rank

	return predicted