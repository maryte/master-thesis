#!/usr/bin/env python
from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt
import sqlite3
import glob
import xml.etree.ElementTree as ET
import omdb
import untangle
import random
import operator
import parameters
from sklearn.metrics import mean_squared_error
from rec_evaluation import evaluation, ap_evaluation
from cf_get_rec import cf_rec
from ap_get_rec import ap_rec
from cfap_get_rec import cfap_rec
from apriori import callAPriori
from shannon import sdi
from diversity_calculation import diversity_index_calculation

def pad_imdb_id(imdb):
	if len(str(imdb)) == 9:
		return imdb
	while (len(str(imdb)) < 7):
		imdb = "0" + imdb
	return "tt" + imdb

class XmlDictConfig(dict):
    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself 
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a 
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})

def percentage(whole):
  return int((parameters.PERCENTAGE/100)*float(whole))
		
def main():

	if len (sys.argv) != 5:
		print ("Usage: <cf_*.xls> <eval.xls> <n_users> <mode>")
		sys.exit (1)
		
	input_file = sys.argv[1]
	output_file = sys.argv[2]
	n_users = int(sys.argv[3])
	#mode = 1 only CF, mode = 2 only AR, mode = 3 CF-AR-DIV
	mode = int(sys.argv[4])
	
	# Create a SQLite database for the results.
	db_filename = 'RS/test.db' # TODO: parameters.SET this from user input.
	conn = sqlite3.connect(db_filename)
	
	my_ratings = {}
	
	cf_predicted_len = 0
	cf_precision = 0
	cf_recall = 0
	cf_lift = 0
	cf_norm_rmse = 0
	cf_true_n_users = 0
	cf_predicted_percentage = {}
	
	ap_predicted_len = 0
	ap_precision = 0
	ap_recall = 0
	ap_lift = 0
	ap_norm_rmse = 0
	ap_true_n_users = 0
	apriori_items, apriori_rules = callAPriori()
	
	cfap_predicted_len = 0
	cfap_precision = 0
	cfap_recall = 0
	cfap_lift = 0
	cfap_norm_rmse = 0
	cfap_true_n_users = 0
	
	div_predicted_len = 0
	div_precision = 0
	div_recall = 0
	div_lift = 0
	div_norm_rmse = 0
	div_true_n_users = 0

	for user_id in range(1,(n_users+1)):

		my_rat = conn.execute("SELECT imdb_id,rating from reviews where userId =" + str(user_id))
		#dictionary movie-rating
		for row in my_rat:
			my_ratings[str(row[0])] = str(row[1])		
		#print ("MY_rat of user "+str(person)+": "+str(len(my_ratings)))
		removed = {}
		for i in range(percentage(len(my_ratings))):
			rc = random.choice(list(my_ratings))
			x = (my_ratings.pop(rc))
			removed[rc]=x
						
		#######################PURE CF#######################
		
		if (mode == 1 or mode == 3):
			cf_predicted = cf_rec(user_id,my_ratings,input_file)
			#radomly extract 20 elements
			cf_predicted_percentage = dict(sorted(cf_predicted.items(), key=operator.itemgetter(1), reverse=True)[:parameters.CF_SET])
			#print ("CF PREDICTED parameters.SET:",cf_predicted_parameters.SET)
			cf_ps,cf_pre,cf_recl,cf_l,cf_nr,cf_tu = evaluation(cf_predicted_percentage,removed,cf_predicted_percentage)
			cf_predicted_len += cf_ps
			cf_precision += cf_pre
			cf_recall += cf_recl
			cf_lift += cf_l
			cf_norm_rmse += cf_nr
			cf_true_n_users += cf_tu
		
		#######################PURE APRIORI#######################

		if (mode == 2 or mode == 3):
			apriori = ap_rec(user_id,my_ratings,input_file,apriori_items, apriori_rules)
			#print ("PURE APRIORI: ",apriori)
			#max n element of the predicted dictionary
			newAp = dict(sorted(apriori.items(), key=operator.itemgetter(1), reverse=True)[:parameters.AR_SET])
			#print("New ap len:" ,len(newAp))
			ap_ps,ap_pre,ap_recl,ap_l,ap_tu = ap_evaluation(removed,apriori)
			ap_predicted_len += ap_ps
			ap_precision += ap_pre
			ap_recall += ap_recl
			ap_lift += ap_l
			ap_true_n_users += ap_tu
		
		#######################CF-APRIORI#######################

		if (mode == 3 or mode == 4):
			cfap_predicted,cfap_recpriori = cfap_rec(user_id,my_ratings,input_file,apriori_items, apriori_rules)
			#max n element of the predicted dictionary
			newAp = dict(sorted(cfap_recpriori.items(), key=operator.itemgetter(1), reverse=True)[:parameters.CFAR_SET])
			#print("New ap len:" ,len(newAp))
			cfap_ps,cfap_pre,cfap_recl,cfap_l,cfap_nr,cfap_tu = evaluation(newAp,removed,cfap_predicted)
			cfap_predicted_len += cfap_ps
			cfap_precision += cfap_pre
			cfap_recall += cfap_recl
			cfap_lift += cfap_l
			cfap_norm_rmse += cfap_nr
			cfap_true_n_users += cfap_tu
		
		#######################DIVERSIFICATION#######################

			rev = newAp
			genre_dict = {}
			year_dict = {}
			language_dict={}
			country_dict={}
			director_dict={}
			actors_dict={}
			genre_list = {}
			year_list = {}
			language_list={}
			country_list={}
			director_list={}
			actors_list={}
			
			diversification_dict = {}

			genre_sdi,year_sdi,language_sdi,country_sdi,director_sdi = diversity_index_calculation(user_id)
			#print ("\n\nDiversity indexes:",genre_sdi,year_sdi,language_sdi,country_sdi,director_sdi)
			
			for imdb_id in rev:
			
				id = pad_imdb_id(imdb_id)
				diversification_dict.setdefault(imdb_id,0)

				#query to the external omdb
				res = omdb.request(i=id, r='xml', tomatoes=True)
				xml_content = res.content
				#conversion from xml to dictionary
				root = ET.XML(xml_content)
				xmldict = XmlDictConfig(root)
				if genre_sdi > 4:
					genre_list = xmldict['movie']['genre'].split(",")
				if year_sdi > 4:
					year_list = xmldict['movie']['year'].split(",")
				if language_sdi > 4:
					language_list = xmldict['movie']['language'].split(",")
				if country_sdi > 4:
					country_list = xmldict['movie']['country'].split(",")
				if director_sdi > 4:
					director_list = xmldict['movie']['director'].split(",")
				#actors_list = xmldict['movie']['actors'].split(",")
				#print (i,". ", xmldict['movie']['title'], ": ",genre_list )
				
				for g in genre_list:
					genre_dict.setdefault(g.strip(),0)
					genre_dict[g.strip()] += 1
					diversification_dict[imdb_id] += genre_dict[g.strip()]
				for y in year_list:
					y_ = str(round(int(y)/10))
					year_dict.setdefault(y_+"0",0)
					year_dict[y_+"0"] += 1
					diversification_dict[imdb_id] += year_dict[y_+"0"]
				for l in language_list:
					language_dict.setdefault(l.strip(),0)
					language_dict[l.strip()] += 1
					diversification_dict[imdb_id] += language_dict[l.strip()]
				for c in country_list:
					country_dict.setdefault(c.strip(),0)
					country_dict[c.strip()] += 1
					diversification_dict[imdb_id] += country_dict[c.strip()]
				for d in director_list:
					director_dict.setdefault(d.strip(),0)
					director_dict[d.strip()] += 1
					diversification_dict[imdb_id] += director_dict[d.strip()]

				
			# #print ("genre dict:",genre_dict)
			# if genre_sdi > 4:
				# print ("genre sdi:",sdi(genre_dict))
			# #print ("year dict:",year_dict)
			# if year_sdi > 4:
				# print ("year sdi:",sdi(year_dict))
			# #print ("language dict:",language_dict)
			# if language_sdi > 4:
				# print ("language sdi:",sdi(language_dict))
			# #print ("country dict:",country_dict)
			# if country_sdi > 4:
				# print ("country:",sdi(country_dict))
			# #print ("director dict:",director_dict)
			# if director_sdi > 4:
				# print ("director sdi:",sdi(director_dict))
			#print ("actors dict:",actors_dict)
			#print ("actors sdi:",sdi(actors_dict))
			
			diversified_set = dict(sorted(diversification_dict.items(), key=operator.itemgetter(1), reverse=False)[:parameters.DIV_SET])
			div_ps,div_pre,div_recl,div_l,div_nr,div_tu = evaluation(diversified_set,removed,cfap_predicted)
			div_predicted_len += div_ps
			div_precision += div_pre
			div_recall += div_recl
			div_lift += div_l
			div_norm_rmse += div_nr
			div_true_n_users += div_tu
	
	
	#######################RESULTS#######################
	
	if (mode == 1 or mode == 3):
		print("\n######################",(n_users))
		print("\n*******************PURE CF")
		print("set len:",cf_predicted_len/n_users)
		print("precision:",cf_precision/cf_true_n_users)
		print("recall:",cf_recall/cf_true_n_users)
		print("lift:",cf_lift/cf_true_n_users)
		print("norm_rmse:",cf_norm_rmse/cf_true_n_users,"%")
	
	if (mode == 2 or mode == 3):
		print("\n*******************PURE APRIORI # rules:",len(apriori_rules))
		print("set len:",ap_predicted_len/n_users)
		print("precision:",ap_precision/ap_true_n_users)
		print("recall:",ap_recall/ap_true_n_users)
		print("lift:",ap_lift/ap_true_n_users)
	
	if (mode == 3):
		print("\n*******************CF-APRIORI")
		print("set len:",cfap_predicted_len/n_users)
		print("precision:",cfap_precision/cfap_true_n_users)
		print("recall:",cfap_recall/cfap_true_n_users)
		print("lift:",cfap_lift/cfap_true_n_users)
		print("norm_rmse:",cfap_norm_rmse/cfap_true_n_users,"%")
			
		print("\n*******************DIVERSIFICATION")
		#print("diversified_set len:",len(diversified_set))
		#print("diversified_set:",diversified_set)
		print("set len:",div_predicted_len/n_users)
		print("precision:",div_precision/div_true_n_users)
		print("recall:",div_recall/div_true_n_users)
		print("lift:",div_lift/div_true_n_users)
		print("norm_rmse:",div_norm_rmse/div_true_n_users,"%")
			
	
	

	
	
if __name__ == "__main__":
    main()