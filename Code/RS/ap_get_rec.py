#!/usr/bin/env python
from math import sqrt
import sys
import xlrd
import numpy as np
import xlwt
import sqlite3
import glob
import xml.etree.ElementTree as ET
import omdb
import untangle
import random
import operator
from sklearn.metrics import mean_squared_error
from rec_evaluation import evaluation
from apriori import callAPriori
import parameters

def percentage(whole):
  return int((parameters.PERCENTAGE/100)*float(whole))

def ap_rec(person,my_ratings,input_file,apriori_items, apriori_rules):
	
	apriori = {}
	
	#if the pre in the user's reviews
	for rule in apriori_rules:
		if all((pre in my_ratings) for pre in rule[0][0]): #all pre in myratings
			for post in rule[0][1]:
				if apriori.get(post) == None :
					apriori.setdefault(post,1)
				else:
					apriori[post] = apriori[post] + 1
	
	#if the whole subgroup in the set
	for item in apriori_items:
		if all((i in apriori) for i in item[0]):
			for i in item[0]:
				apriori[i] = apriori[i] * item[1]
				
	#print ("len pure apriori",len(apriori))
	return apriori