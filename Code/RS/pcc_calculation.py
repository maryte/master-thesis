import sys, glob, sqlite3
import xml.etree.ElementTree as ET
# import omdb
# import untangle
import numpy as np
#import scipy.stats
from scipy.stats import pearsonr

class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)


class XmlDictConfig(dict):
    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself 
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a 
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})

def pcc(id_1,id_2):

	# Create a SQLite database for the results.
	db_filename = 'test.db' # TODO: Set this from user input.
	conn = sqlite3.connect(db_filename)

	#query to the internal sqlite db
	#movie rated by both
	cursor1 = conn.execute("SELECT imdb_id,rating from reviews where userId =" + id_1 ) 
	cursor2 = conn.execute("SELECT imdb_id,rating from reviews where userId =" + id_2 ) 
	
	#dictionary movie-rating
	q1 = {}
	q2 = {}
	for row in cursor1:
		q1[str(row[0])] = str(row[1])
	for row in cursor2:
		q2[str(row[0])] = str(row[1])

	#find common rated movies
	keys_a = set(q1.keys())
	keys_b = set(q2.keys())
	intersection = keys_a & keys_b # '&' operator is used for set intersection
	
	if len(intersection) <= 2:
		#print ("id1: "+id_1+", id2: "+id_2+" not enough common review")
		return [None,None]
	
	#list ratings of common rated movies
	rat1 = []
	rat2 = []
	for i in intersection:
		rat1.append(float(q1[i])) 
		rat2.append(float(q2[i]))

	#print (rat1)
	#print (rat2)
	try:
		return (pearsonr(rat1, rat2))
	except:
		#print ("id1: "+id_1+", id2: "+id_2)
		#print (rat1)
		#print (rat2)
		return [None,None]


				
def main():
	# TODO: Replace this with getopt.
	if 2 == len(sys.argv) and '--help' == sys.argv[1]:
		print('USAGE:')
		print(sys.argv[0], '[id_1] [id_2]')
		
	if len(sys.argv) != 3:
		print('USAGE:')
		print(sys.argv[0], '[id_1] [id_2]')
		exit(0)

	id_1 = sys.argv[1]
	id_2 = sys.argv[2]
	
	print (pcc(id_1,id_2))
	
	

	

if __name__ == '__main__':
    main()
