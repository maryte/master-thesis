import sys, glob, sqlite3
import xml.etree.ElementTree as ET
import omdb
import untangle
import operator
from random import randint	
from shannon import sdi

class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)


class XmlDictConfig(dict):
    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself 
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a 
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})

def pad_imdb_id(imdb):
	while (len(str(imdb)) < 7):
		imdb = "0" + imdb
	return "tt" + imdb
	
				
def diversity_index_calculation(userId):
	
	# Create a SQLite database for the results.
	db_filename = 'test.db' # TODO: Set this from user input.
	conn = sqlite3.connect(db_filename)

	#query to the internal sqlite db
	cursor = conn.execute("SELECT imdb_id,rating from reviews where userId=" + str(userId))
	
	rev = {}
	for row in cursor:
		rev[str(row[0])] = str(row[1])
	
	#print ("\n\nrev:",len(rev))
	genre_dict = {}
	year_dict = {}
	language_dict={}
	country_dict={}
	director_dict={}
	actors_dict={}
	
	i = 0
	for imdb_id in rev:
		id = pad_imdb_id(imdb_id)

		#query to the external omdb
		res = omdb.request(i=id, r='xml', tomatoes=True)
		xml_content = res.content
		#conversion from xml to dictionary
		root = ET.XML(xml_content)
		xmldict = XmlDictConfig(root)
		genre_list = xmldict['movie']['genre'].split(",")
		year_list = xmldict['movie']['year'].split(",")
		language_list = xmldict['movie']['language'].split(",")
		country_list = xmldict['movie']['country'].split(",")
		director_list = xmldict['movie']['director'].split(",")
		actors_list = xmldict['movie']['actors'].split(",")
		#print (i,". ", xmldict['movie']['title'], ": ",genre_list )
		
		for g in genre_list:
			genre_dict.setdefault(g.strip(),0)
			genre_dict[g.strip()] += 1
		for y in year_list:
			y_ = str(round(int(y)/10))
			year_dict.setdefault(y_+"0",0)
			year_dict[y_+"0"] += 1
		for l in language_list:
			language_dict.setdefault(l.strip(),0)
			language_dict[l.strip()] += 1
		for c in country_list:
			country_dict.setdefault(c.strip(),0)
			country_dict[c.strip()] += 1
		for d in director_list:
			director_dict.setdefault(d.strip(),0)
			director_dict[d.strip()] += 1
		for a in actors_list:
			actors_dict.setdefault(a.strip(),0)
			actors_dict[a.strip()] += 1

			
	genre_sdi = sdi(genre_dict)
	year_sdi = sdi(year_dict)
	languade_sdi = sdi(language_dict)
	country_sdi = sdi(country_dict)
	director_sdi = sdi(director_dict)
	#actors_sdi =
		
	#print ("genre dict:",genre_dict)
	#print ("genre sdi:",sdi(genre_dict))
	#if genre_sdi <3:
	#		print(dict(sorted(genre_dict.items(), key=operator.itemgetter(1), reverse=True)[:3]))
	#print ("year dict:",year_dict)
	#print ("year sdi:",sdi(year_dict))
	#if year_sdi <3:
	#		print(dict(sorted(year_dict.items(), key=operator.itemgetter(1), reverse=True)[:3]))
	#print ("language dict:",language_dict)
	#print ("language sdi:",sdi(language_dict))
	#if languade_sdi <3:
	#		print(dict(sorted(language_dict.items(), key=operator.itemgetter(1), reverse=True)[:3]))
	#print ("country dict:",country_dict)
	#print ("country:",sdi(country_dict))
	#if country_sdi <3:
	#		print(dict(sorted(country_dict.items(), key=operator.itemgetter(1), reverse=True)[:3]))
	#print ("director dict:",director_dict)
	#print ("director sdi:",sdi(director_dict))
	#if director_sdi <3:
	#		print(dict(sorted(director_dict.items(), key=operator.itemgetter(1), reverse=True)[:3]))
	#print ("actors dict:",actors_dict)
	#print ("actors sdi:",sdi(actors_dict))
	#if sdi(actors_dict)<3:
	#		print(dict(sorted(actors_dict.items(), key=operator.itemgetter(1), reverse=True)[:2]))
	
	# print ("\n\nSearch test:")
	# res = omdb.search('2000')
	# print(res[0].title,res[0].imdb_id)

	# print ("\n\nRandom movie test:")
	# res = omdb.imdbid(pad_imdb_id(str(randint(0,2155529))))
	# print (res.imdb_id, res.title)
	
	return (genre_sdi,year_sdi,languade_sdi,country_sdi,director_sdi)

	
				
def main():
	# TODO: Replace this with getopt.
	if 2 == len(sys.argv) and '--help' == sys.argv[1]:
		print('USAGE:')
		print(sys.argv[0], '[userId]')
		
	if len(sys.argv) != 2:
		print('USAGE:')
		print(sys.argv[0], '[userId]')
		exit(0)

	userId = sys.argv[1]
	
	res = diversity_index_calculation(userId)
	
	
	
	#print ("Operation done successfully");

if __name__ == '__main__':
    main()
