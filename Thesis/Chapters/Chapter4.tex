% Chapter 1

\chapter{Experimental Tuning and Evaluation} % Main chapter title

\label{Chapter4} % For referencing the chapter elsewhere, use \ref{Chapter1} 

In the past decade, there has been a vast amount of research in the field of recommender systems,
mostly focusing on designing new algorithms for increasing the performance. An application designer
who wishes to add a recommendation system to a service provider, has a large variety of algorithms at disposal and he/she have to make a decision about the most appropriate one taking in account the properties to be be achieved in the concerned application.

There are many different performance measures that can be found in the RSs' field literature and 
research papers. Usually, various indexes are employed to evaluate the same
system, because different quality properties need to be analysed. Due to the fact that performance measures are often quite complex and hard to interpret, they constitute an research area themselves.

Our evaluations were all computed off-line and performed by exploiting a pre-collected data set of reviews that we used to simulate the behaviour of the users interacting with the system. In doing this, we assumed that the past user's behaviour will be similar enough to the one he will assume in the future, so that we can make reliable decisions based on that. 

Even if, in this setting, recommender's influence cannot directly be measured on user interactions, this technique has been useful to filter out inappropriate approaches and study the system reactions with respect to different parameters configurations.

The technique we adopted only focused on improving the quality of predictions made while, aspects such as the optimization of the algorithm in both time and memory space, are left outside the realm of this evaluation.

This chapter shows the results of the tests executed to tune our system and to evaluate its performance.
In the next subsections, we will outline the used datasets, the adopted testing techniques and metrics. Finally, we will discuss the main obtained results.

\section{Datasets}

\subsection{Transactions}

A transaction can be generally referred as a recorded interaction between a user and and an item and the whole set of transactions is often used by the systems to infer preferences profiles. 

For our purposes, we exploited the rating data set made available by the BAGMovies start-up, collected by their online platform. The dataset was collected until august 2015 and it counts 1048576 reviews belonging to 7120 users. The data were provided in an $Excel$ [$data.xls$] format and we parsed it in an $SQLite$ database \cite{sqlite}, version 3.13.0, to make them easily accessible through the code.
The $review$ table is of the shape $<user\_id, movie\_id, rate>$ and the movie rate is represented by a digit from 0 to 5. We pre-processed the data to only include users and movies with significant rating history (i.e., users who rated at least 30 movies).

\subsection{Movies}

In order to access details about the items, we exploited Internet Movie Database ($IMDB$) \cite{imdb}, by far the most extensive collection of movie information available nowadays. 
It contains 3,813,412 titles and 7,300,466 people among actors, directors and miscellaneous crew \cite{imdbstats}.

Movie features can be retrieved directly from the IMDB website or accessed directly from a copy of the data on the local system.

In order to easily access it, we exploited an open Pyhton wrapper, built around The Open Movie Database API, called $omdb.py $\cite{omdb}, not endorsed by or affiliated with $OMDbAPI.com$ \cite{omdb}.

\section{Evaluation}

\subsection{Performance Measures}

As we said in Chapter 2, we can categorize previously suggested RSs into a number of major groups, each corresponding to a different task. Our system can be positioned under the umbrella of the prediction of user opinion over a set of items and, traditionally, it must be evaluated on the Accuracy of
these predictions. This is the most common scenario in the evaluation of regression and classification
algorithms in the machine learning and statistics literature \cite{evalms}.

Nevertheless, during the course of the whole implementation phase, we adopted different metrics in order to provide different rankings of the proposed algorithms:
\begin{itemize}
\item \textit{RMSE.} The Root of the Mean Square Error is a traditional metric in the prediction task field and it was also chosen for the famous Netflix Prize evaluation. It is a good measure of Accuracy and represents the sample standard deviation of the differences between predicted values and observed values, measuring how close the predictions are to the values actually observed. If $c_{ij}$ is the predicted rating for user $i$ over item $j$, $\bar{c}_{ij}$ is the true rating, and
$N = {(i, j)}$ is the set of user-item ratings, then the RMSE is defined as:
$$RMSE = {\sqrt {\frac{1} {N}{\sum\limits_{i = 1}^N {(c_{ij} - \bar{c}_{ij} } })^{2} } }$$
The RMSE is a quadratic scoring rule, which is most useful when large errors are particularly undesirable. Furthermore, it is particularly suitable for the prediction task, because it measures inaccuracies on all ratings, either negative or positive. For this purpose, I exploited the $mean\_squared\_error$ method provided by the $sklearn.metrics$ Python library. 
\item \textit{Precision.} This value is typically used to evaluate those systems aiming to provide, given an existing list of items that were viewed, a list of additional items that the user may want to visit. In our scenario the situation is not symmetric: the task of the system is to suggest good items, not to discourage the use of bad items. The precision is defined as follows:
$$precision =  \frac{\#true\ positives} {\#true\ positives + \#false\ positives}$$
In our application domain, the precision measure describes what proportion of the recommendations is actually suitable for the user out of the whole size of the suggested set. 
\item \textit{Recall.} This measure is often associate to the Precision quantity in order to find a trade-off between the two. For example, while allowing longer recommendation lists typically improves Recall, it is also likely to reduce the Precision and vice versa. It is defined as:
$$recall =  \frac{\#true\ positives} {\#true\ positives + \#false\ negative}$$
In our case it represents the items correctly suggested out of the test-set the system aimed to predict.
\item \textit{Lift.} This measure is just a Recall calculated only on the higher rated items, i.e. the subset of the test set the system is actually interested in predicting.
$$lift =  \frac{\#true\ positives > N} {\#true\ positives > N + \#false\ negative > N}$$
\end{itemize}

\subsection{Experimental Results}

As already mentioned, our evaluations are based on off-line experiments, comparing the performance of the three presented algorithms in a number of different candidate settings over the real data. 
For all the experiments, we randomly removed the 20\% of ratings by the target user's history, to be adopted as test-set, and asked the system to compute the prediction and evaluate the statistics on them. The data reported on the followings plots are the arithmetic means of the results obtained on the whole set of users.

In the evaluation procedure explained above we tried to experimentally tune the main parameters affecting the performances of system through the comparison of the system in different settings.
The parameters we analysed are:
\begin{itemize}
\item \textit{Number of closer users} to consider in the CF approach.
\item \textit{Minimum Support and Confidence} in the ARs mining.
\end{itemize}

In the next subsections, we will report the results of our experiments in the different settings.

\subsubsection{Number of closer users tuning}

We analysed the performance of the CF approach with respect to the number of closer users, with respect to the target one, we consider to build the CF-set. In order to evaluate the result we considered the Accuracy, Precision, Recall and Lift measures. 

In Figure \ref{fig:CFusers} is shown the plot comparing them.

\begin{figure}[htb]
\centering
\resizebox{10cm}{!}{\input{Plots/cf.tiks}}
\caption{Number of closer users for CF-set}\label{fig:CFusers}
\end{figure}

As can be seen, the Accuracy, calculated through the RMSE index, tends to be better when the number of user considered is 10. The result obtained with a low number of user can be explained thinking that they are not enough to compute a significant mean among opinions. On the other side, considering an higher number of user leads to an high error because they are not similar enough.

\subsubsection{Minimum Support and Confidence tuning}

Since the AR approach does not allow us to predict review scores, here we considered our recommendation problem as the task to simply predict items high rated items in the test-set. For this reason we could not use the Accuracy, since we do not have the predicted score quantity, not the Lift, since we already aim to predict only the highest scored items. Instead, we adopted Precision and Recall, giving priority to the latter since, as already said, our goal is suggest items in the test-set and not to discourage the use of the others.

Intuitively is it necessary to find a trade-off between the number of association patterns necessary to build the set and their strength, namely, their significance in the considered data-set. 

In Table \ref{table:ARnumber} can be seen the number of association patterns mined with the relative values of minimum $Confidence$ and $Support$.

\begin{table}
\centering
\begin{tabular}{ c | c | c | c }
$Conf_{min}$ & 0.4 & 0.5 & 0.6 \\
$Supp_{min}$	&	& 	&		\\ \hline
0.04 	&17640	&16256	&13914	\\ \hline
0.05 	&1263	&1014 	&794		\\ \hline
0.06 	&279	&218	&164		\\ \hline
0.07 	&107	&89 	&66		\\ \hline
0.08 	&66		&56 	&45		\\ \hline
0.09 	&33		&30 	&26	\\ \hline
0.1 	&23		&20 	&17		\\ \hline
\end{tabular}
\caption{Rules number}
\label{table:ARnumber}
\end{table}

In Table \ref{table:ARcs} can be seen the how Precision and Recall vary with the considered minimum $Condifence$ and $Support$. 

\begin{table}
\centering
\begin{tabular}{ c | c | c | c  }
$Conf_{min}$ & 0.4 & 0.5 & 0.6 \\
$Supp_{min}$	&	& 	&		\\ \hline
0.04 	&p:	8.26	&p: 8.23 	&p: 8.07	\\ 
		&r:	11.68	&r: 11.19	&r: 9.39	\\ \hline
0.05 	&p: 9.62	&p: 9.83 	&p: 9.13	\\ 
		&r: 7.85	&r: 6.05 	&r: 5.11	\\ \hline
0.06 	&p: 12.72	&p: 14.52 	&p: 16.28	\\ 
		&r: 5.02	&r: 4.11 	&r: 4.02	\\ \hline
0.07 	&p: 16.30	&p: 15.01 	&p: 22.32	\\ 
		&r: 3.74	&r: 2.67 	&r: 3.09	\\ \hline
0.08 	&p: 16.62	&p: 16.18 	&p: 22.00	\\ 
		&r: 2.65	&r: 2.68 	&r: 2.64	\\ \hline
0.09 	&p: 20.51	&p: 29.95 	&p: 26.19	\\ 
		&r: 2.02 	&r: 1.94 	&r: 2.14	\\ \hline
0.1 	&p: 26.53	&p: 25.32 	&p: 26.01	\\
		&r: 1.91	&r: 1.75 	&r: 1.53	\\ \hline 
\end{tabular}
\caption{Precision and Recall values}
\label{table:ARcs}
\end{table}

The data reported show how the number of the mined rules strongly decreases with the increasing of the minimum Support, determining a Precision growth. This is due to the fact that the rules obtained in this way are few but highly significant and they are able to generate only a small, but extremely reliable, recommended set.
At the same time we can notice that it also decrease the Recall values since the number of predicted items obtained in this way is very low.

On the other side, we it can be seen how keeping low Minimum Support value the Precision value remains low due to the big resulting set. 

This data show that the AR-approach itself is not the best approach for our specific recommendation task where we want, ideally, concentrate in a low number of result an high Recall value.

\subsubsection{Mixed strategy evaluation}

In doing the evaluation of the overall mixed strategy we set the number of $closer$ $users$ to be considered in the CF-phase at 10, value discovered to be the best trough previous experiments from an Accuracy point of view, $minSupport$ at 0.7 and $minConfidence$ at 1 to find a trade-off between the higher values of Precision and Recall.

In Table \ref{table:MX} is shown the comparison of the pure CF and AR strategies with the mixed CF-AR one and with the final set obtained after the Diversification-phase.
The reported values represent the mean of the measures evaluated testing the system on all whole user set present in the database.

As can be seen, the CF-AR mixed approach seems to achieve better performance than the previous two.
On the other hand, unsurprisingly, even if still offering better performance than the pure strategies, the Diversification approach determine a performance decrease with respect to the CF-AR mixed one. Futher studies will interest the evaluation of the quality of the recommended set in terms of proposal diversity. 

\begin{table}
\centering
\begin{tabular}{ c | c | c | c | c  }
 & Set-size [mean] & RMSE[\%] & Precision[\%] & Recall[\%] \\ \hline
CF-SET 		& 50 & 19.78	& 7		& 4	\\ \hline
AR-SET		& 5  & -		& 46	& 1.98	\\ \hline
CF-AR-SET 	& 50 & 17.6		& 16.26 & 14.26	\\ \hline
DIV-SET 	& 20 & 18.84	& 17.33 & 7.33	\\ \hline
\end{tabular}
\caption{Performance comparison}
\label{table:MX}
\end{table}




