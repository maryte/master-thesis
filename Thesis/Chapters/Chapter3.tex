
\chapter{System Overview: a Hybrid Recommender System} % Main chapter title

\label{Chapter3} % For referencing the chapter elsewhere, use \ref{Chapter1} 

In this chapter we will describe the problem we wanted to address and we will furnish the details of the developed algorithm.

\section{Problem Description}

As mentioned in the introductory chapter, this study was born from a collaboration with the BAGMovies \cite{bagmovies} company. The initial focus of the research was to improve both Accuracy and Quality of the Recommendation System (RS) that was running on the existing online platform trivially showing the most popular contents reviewed by the target's friends.

This experience stimulate us to investigate in the movies recommendation direction.

The objective of this thesis was to design a specific hybrid recommender for movies able to achieve better prediction quality than the one reachable through the previous approach. For this purpose we exploited both rating information and content features of the observed items. 

\section{Design and Methodology}

The implementation steps can be categorized into four main components:
\begin{itemize}
\item \textit{Collaborative Filtering phase.} Make candidate selection looking for the contents rated by the most similar users.
\item \textit{Association Rules phase.} Make candidate selection analysing the reviews available in the database and inferring relation among items.
\item \textit{Merging phase.} Re-rank the set obtained through the CF phase (the CF-set) using support and confidence information among items found during the AR phase.
\item \textit{Diversification/Accuracy trade-off phase.} Analyse the history of the user to understand how much he/she appreciates novelty and adapt the final recommended list on it.
\end{itemize} 
The described functional architecture is illustrated in Figure \ref{fig:fa}.

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{../Plots/functional_arc.pdf}
    \caption{Functional architecture of the system}
    \label{fig:fa}
\end{figure}

We only focused on improving the quality of predictions made while, optimizing the algorithm in both time and memory space, is outside the realm of this project and it is left as a future improvement. Nevertheless, the software can be splitted in two macro-sections: an on-line one, computed at the actual user request, and an off-line one, big computations such as the construction of the Similarity Matrix or the Association Rules mining that can be computed in advance with respect to the actual user request. Since they represent information that not change very often, it does not negatively affect the result of the prediction.

\subsection{Collaborative Filtering}

As already mentioned in Chapter 2, Collaborative Filtering (CF), also referred to as social filtering, filters information by using the recommendations of other people. It is based on the idea that people who agreed in their evaluation of certain items in the past are likely to agree again in the future. For example, in the real world, a person who wants to see a movie might ask for recommendations from friends. The recommendations of some friends who have similar interests are trusted more than recommendations from others. The same kind of information is used in our algorithm to make decision regarding which movie to see.

We can systematise the the whole adopted procedure in the following steps:
\begin{itemize}
\item \textit{Creation of the User Similarity Matrix.} The code analyses the database and extract the common reviews among users taking the information in the reviews' table to calculate the similarity between each pair of them through the Pearson Correlation Coefficient (PCC). For this purpose we exploited the $pearsonr$ function provided by the $scipy.stats$ module, implementing the Pearson similarity index measuring the linear relationship between two datasets (refer to Chapter2). Each $PCC(ij)$ stating the similarity relation between user $i$ and $j$, is saved populating the Similarity Matrix in the position $M[i][j]$. Due to the fact that the User Similarity Matrix is symmetric, the actual implementation only calculates the upper triangular part, which includes all entries above the main diagonal.
To complete all entries, it is enough to add the transposed similarity matrix to to the actual one. 
This calculation can be computed off-line, before the actual recommendation request. 
\item \textit{Identification of the $N$ closer users.} Selection of the users with the most similar tastes to the target one, namely the ones with higher PCC value.
\item \textit{CF-set computation.} List the items the $N$ trustable users liked (except those already evaluated by the target one) and score computing a mean of all the obtained scores weighted with the closeness index of each reviewer. The procedure to calculate the CF-set is showed in the Algorithm \ref{alg:cf}.
\item \textit{Subset extraction.} Randomly select N items among the highest-score ones.
\end{itemize}

\begin{algorithm}
\caption{Collaborative Filtering procedure}\label{alg:cf}
\begin{algorithmic}[1]
\Procedure{CF}{$target,Ncloser$}
\State $totals\gets \{\}$\Comment{sum of score*pcc}
\State $simSum\gets \{\}$\Comment{sum of pcc}
\State $predicted\gets \{\}$
\State $score\gets 0$ \Comment{normalized score}
\For{$user \in Ncloser$}
\For{$(item,rate) \in target.reviews$}	
\If {$item \notin user.reviews$}
\State $total[item]\gets total[item] + rate*pcc(target,user)$
\State $simSum[item]\gets simSum[item] + pcc(target,user)$
\EndIf
\EndFor
\EndFor
\For{$item \in total$}
\State $rate\gets total[item]/simSum[item]$
\State $predicted[item]\gets rate$
\EndFor
\State $predicted\gets Extract_N(predicted)$
\State \textbf{return} $predicted$\Comment{list with normalized scores}
\EndProcedure
\end{algorithmic}
\end{algorithm}
Since the first implementation, it have been clear the limit suffered by this strategy in this particular application:
\begin{itemize}
\item \textit{Dataset depending parameters.} We have no control about the size of the recommended set. It can be either very small in case of high similarity among users or, like in the case of our dataset, extremely huge, thus impractical to be recommended as a whole. It means that the number of closer users to be taken in account and the size of the final set depend on the nature of the dataset. 
\item \textit{There is no score granularity.} The set is composed by items ranked with a predicted score made by a digit from 0 to 5. Many of them have the same score and there is no way to order them an give high priority to some of them.
\item \textit{Classical CF cons.} If no one among the trustable friends rated a movie the system cannot recommend it. Furthermore, it suffers of the cold stard approach and it gives best performances as the number of items rated by the target user increases.
\end{itemize}

For these reasons it was necessary merge this algorithm with a further strategy, able to address the listed problems.

\subsection{Association Rules}

We decided to explore the Association Rules approach (AR) because we were searching for a method to granulate the scores and order the itemsets obtained through the CF approach (the CF-set). In this way, we aimed to produce an algorithm able to restrict the huge set of recommendation with a smarter approach than the mere randomly selection. 

We first implemented the pure algorithm in such a way to have the two software components well defined and to finally easily compare the performances of the two approaches separately.

As mentioned in Chapter 2, given a set of transactions, where each transaction
is a set of items, an Association Rule (AR) is a rule of the form $X \rightarrow Y$, where $X$ and $Y$
are sets of items. The meaning of this rule is that the presence of $X$ in a transaction
implies the presence of $Y$ in the same transaction. $X$ and $Y$ are respectively called
the body and the head of the rule. 

Each rule has two measures: confidence and support. The confidence of the rule is the percentage of transactions that contain $Y$ among transactions that contain $X$; The support of the rule is the percentage of transactions that contain both $X$ and $Y$ among all transactions in the input data
set. In other words, the confidence of a rule measures the degree of the correlation between itemsets, while the support of a rule measures the significance of the correlation between itemsets.

While this sort of approach is largely used in the e-commerce domain, in particular in the basket market analysis, it remains unadopted in the movie recommendation field.

The methodology adopted can be systematized in the following points:
\begin{itemize}
\item \textit{Transitions parsing.} The $reviews$ table in the database is processed to obtain a $.csv$ file grouping in each row the list of all the positively rated items for each user.
\item \textit{ARs mining.} This phase aims to find more frequent positive rated itemsets and relation among them. The algorithm we adopted is based on the Apriori data mining one (refer to Chapter 2) and written editing the Python Implementation of Apriori Algorithm available here \cite{apcode}. Again, both the previous points can be computed off-line since correlation among items is not something that is supposed to change often.
\item \textit{AR-set computation.} Analyze the targets's history and add to the prediction set those items that satisfies a rule (except those rules deriving from the reviews of the same user) and rank each of them with respect to normalized confidence and support. In Algorithm \ref{alg:ar} can be seen the pseudo-code of the computation of the AR-set.
\item \textit{Subset extraction.} Select the N items with highest value in the set.
\end{itemize}

\begin{algorithm}
\caption{AR reccomendation strategy}\label{alg:ar}
\begin{algorithmic}[1]
\Procedure{CF}{$target,APitemsets,APrules$}
\State $predicted\gets \{\}$
\For{$rule \in APrules$}\Comment{$for\ each\ rule$}
\If {$(all\ items \in rule.body) \in target.reviews$}
\For{$p \in rule.head$}
\State $predicted[p]\gets predicted[p] + (rule.confidence*100)$
\EndFor
\EndIf
\EndFor
\For{$itemset \in APitemsets$} \Comment{$for\ each\ itemset$}
\If {$(all\ items \in itemset) \in predicted$}
\For{$i \in item$}
\State $predicted[i]\gets predicted[i] * (item.support*100)$
\EndFor
\EndIf
\EndFor
\State $predicted\gets ExtractNmax(predicted)$
\State \textbf{return} $predicted$\Comment{recommended list}
\EndProcedure
\end{algorithmic}
\end{algorithm}
Like the previous case, also this approach presented some limitations:
\begin{itemize}
\item \textit{Dataset depending parameters.} The minConfidence and minSupport used to find the rules strongly depend on the nature of the dataset. It is necessary to find a trade-off between how many rules are useful and how strong they should have to obtain good suggestions.
\item \textit{The recommended list has no score value.} The code can only select which item could be interesting for the user but it cannot establish a predicted score nor there is a way to sort the obtained AR-set.
\item \textit{Classical ARs cons.} Again, if no one among the users in the database rated a movie the system cannot recommend it. Furthermore, it suffers of the cold stard approach and it gives best performances as the number of items rated by the target user increases.
\end{itemize}

\subsection{Mixed strategy}

There have been many examples in recommender systems literature where an effective technique
was developed by combining or integrating several other recommendation techniques that help to avoid certain limitations that individual approaches may have. 

Following this idea, we explored the possibility to combine the two approaches presented in the previous sections and proposed a ranking technique able to granulate the list obtained through the CF approach and adding the support and confidence information to the final recommended set in order to restrict it.

The kind of reasoning behind this strategy is based on the fact that we want to prioritize the items more rated than the other and those that satisfies AR derived by all the dataset reviews, and not only by users with similar tastes.

The designed strategy can be summarized in the following steps:
\begin{itemize}
\item \textit{CF-set computation.} Obtain the initial CF set from the closest N users like explained in Section 2.2.1.
\item \textit{AR mining.} Derive offline subitems and AR with higher support and confidence than the specified minimum values like explained in the previous Section 2.2.2.
\item \textit{CF-set ranking.} Recompute the score of the itemsets in the CF-set taking in account itemsets' support and satisfied rules' confidence as shown in Algorithm \ref{alg:apcfcode}
\item \textit{Take better scores' items.} Once the CF-set is sorted we can again extract the highest ranked items and obtain the CF-AR-subset.
\end{itemize}

\begin{algorithm}
\caption{CF-AR mixed strategy}\label{alg:apcfcode}
\begin{algorithmic}[1]
\Procedure{CF-AR}{$target,CFset,APitemsets,APrules$}
\State $predicted\gets \{\}$
\For{$rule \in APrules$}\Comment{$for\ each\ rule$}
\If {$(all\ item \in rule.body) \in target.reviews$}
\If {$(all\ p \in rule.head) \in CFset$}
\For{$p \in rule.head$}
\State $predicted[p]\gets predicted[p] + (rule.confidence*100)$
\EndFor
\EndIf
\EndIf
\EndFor
\For{$itemset \in APitemsets$} \Comment{$for\ each\ itemset$}
\If {$(all\ item \in itemset) \in predicted$}
\For{$i \in item$}
\State $predicted[i]\gets predicted[i] * (item.support*100)$
\EndFor
\EndIf
\EndFor
\State $predicted \gets ExtractNmax(predicted)$
\State \textbf{return} $predicted$\Comment{recommended list}
\EndProcedure
\end{algorithmic}
\end{algorithm}

To summarize, the illustrated strategy can bring our system the following advantages:
\begin{itemize}
\item \textit{Sorted CF-set.} Through this technique we can re-rank the CF-set's items, differentiate the items' scores and make the set sortable.
\item \textit{Support and confidence information.} It allow us to prioritize those itemsets appearing to be positively ranked more often than others and those satisfying some AR with respect to the objects already ranked by the user.
\end{itemize}
On the other and, the following disadvantages can be listed: 
\begin{itemize}
\item \textit{A lot of parameters to be tuned.} The whole system performance is strongly affected by the value of the following parameters: number of closer users in the building of the CF-set, minimum support and confidence in the AR mining operation, weights to these parameter in the ordering procedure and how many elements are considered in the final list. All these parameters are context specific and they depend on the dataset nature.
\end{itemize}

\subsection{Diversification}

To complete our system, we built a final layer on the top of our mixed strategy with the intent to improve the quality of the set. 

The consideration from which we started is that a good recommendation must differentiate to be representative. For example, recommend a whole movie trilogy is not probably really effective while, if the user has never saw none of them, it would probably be more useful suggest only the first one.
Despite this, another point to consider in this case is that there could be some features for which a user does not want variety. A thriller's fan would probably be not enthusiastic about being recommended a Disney Pixar's movie. Or, again, a Disney Pixar's fan would not like the first Mickey Mouse. 

For this reason, we decided to explore this aspects designing a system able to understand which are the aspects for which a user is wishful to diversify.

Intuitively, there must be a trade-off between accuracy and diversity: high accuracy may often be obtained by safely recommending to users the most popular ('best-selling') items, which can lead to the reduction in recommendation diversity, but, on the other hand, higher diversity can be achieved by trying to uncover and recommend highly personalized items which are more difficult to predict and,
thus, may lead to a decrease in accuracy \cite{div}. For this reason we decided to design a strategy able to detect users' preferences.

In order to calculate how much a set is diverse, and thus infer how much a user likes the diversity for each particular movie feature analysing his review history, we decided to exploit the Shannon Diversity Index (SDI), popular in the ecological literature and originally proposed by Claude Shannon to quantify the entropy in strings of text. 

The SDI for a user $u$, where $p_i$ is the proportion in which of the feature $f$ assumes the $i-th$ value, can be calculated as follows:

$$SDI_u(f) = -\sum_{i=1}^{F} p_i(f_i) \ln p_i(f_i)$$

Since by definition the $p_i$ will all be between zero and one, the natural log makes all of the terms of the summation negative, which is why we take the inverse of the sum. 

The idea is that the more different letters there are, and the more equal their proportional abundances in the string of interest, the more difficult it is to correctly predict which letter will be the next one in the string. The Shannon entropy quantifies the uncertainty (entropy or degree of surprise) associated with this prediction.
The $SDI$ works well for our purposes because, according its value calculated on a set of movies' features, we can get diversity information. 
In particular, the diversity trend gives high results: 
\begin{itemize}
\item \textit{High value.} In case of high number of different feature values or a fair distribution among features. In both cases, it means high propensity to novelty of the analysed feature.
\item \textit{Low value.} In case of low number of different feature values or an unfair distribution among features. In both cases, it means preference for few values of the analysed feature and thus the user doesn't probably appreciate varying too much. 
\end{itemize}
The steps of our strategies can be described as follows:
\begin{itemize}
\item \textit{Analyse the user's history.} For each significant movie feature, associate a SDI to each user to understand how much he likes the diversification. The attributes we judged as discriminant in the choice of a movie and thus for which we calculated the SDI have been: $$F=\{genre, year, language, country, director\}$$ Again, since the tastes of the user are supposed to not change very often, this computation can be done off-line.
\item \textit{Re-rank according $SDIs_u$.} Using the information derived from the analysis of the user history, we re-ranked the CF-AR-set according the propensity to the novelty of each feature. The details of this process can be seen in Algorithm \ref{alg:divcode}. As can be seen, the $DIV$ method is called by the main one only if the $SDI_t$ associated at the analysed feature $f$ is higher than a decided threshold, namely, only if the user demonstrated to appreciate the diversity for $f$. In this way we re-rank the predicted itemset only for those features for we want to increase novelty. The value of $scores[item]$ increases with the number of items with same value of $f$. At the end of the CF-AR-DIV procedure, we have a list of items higher scored in case there were other items with the same features values. Taking the lower rated items we obtain more diverse items with respect to all the features for which we wanted novelty.
\end{itemize}

\begin{algorithm}
\caption{DIVERSIFICATION strategy}\label{alg:divcode}
\begin{algorithmic}[1]
\Procedure{DIV}{$CFARset,SDI_t(f),scores$}
\State $features\gets \{\}$
\For{$item \in CFARset$}
\State {$features[item.f]\gets features[item.f]+1$}
\State {$scores[item]\gets scores[item]+features[item.f]$}
\EndFor
\State \textbf{return} $\textbf{scores}$\Comment{Recommended list}
\EndProcedure
\end{algorithmic}
\begin{algorithmic}[1]
\Procedure{CF-AR-DIV}{$target,CFARset,SDI_t(F)$}
\State $predicted\gets \{\}$
\State $scores\gets \{\}$
\For{$f \in F$}
\If {$SDI_t(f) > TRESHOLD$}
\State {$scores \gets DIV(CFARset,scores)$}
\EndIf
\EndFor
\State $predicted\gets ExtractNmin(scores)$
\State \textbf{return} $predicted$\Comment{Recommended list,scores}
\EndProcedure
\end{algorithmic}
\end{algorithm}
In this way we can merge the accuracy results obtained with the computation of the CF-AR-set and go on restricting the size of the recommended list focusing on the quality of the items, prioritizing the novelty of the feature for which the user likes diversifications.

The challenges behind this kind of approach are:
\begin{itemize}
\item \textit{Choose good attributes.} We arbitrary chose the attributes through which discriminate a movie and understand the propensity to novelty to the user. 
\item \textit{Correctly tune the parameters.} Like in the previous cases, there are a lot of pararameters to be tuned such as the SDI threshold. 
\end{itemize}

\section{Implementation}

The whole code has been written in the Python programming language, version 3.5.1., since it provides a large amount of useful tools and libraries able to effectively solve a broad set of data analysis problems \cite{pda}. 

In Figure \ref{fig:fs} is shown the project File-System. In the $Code$ folder can be found the $run.sh$ executable file, the $RS$ directory containing the scripts, the $Results$ directory collecting the log files and finally the $README.txt$ file.

In the $RS$ directory can be found the $evaluation\_stats\_def.py$ script, responsible of calling the three implemented algorithms and computing the statistics described in Chapter 4, exploiting the code presents in $rec\_evaluation.py$. The files $cf\_matrix.py$ and $cf\_get\_rec.py$ compute the User Similarity Matrix and the recommendation operations in the CF-phase. $pcc\_calculation.py$ is the script adopted to actually calculate the Pearson Correlation Coefficient and determine the closeness among users. $apriori.py$ and $ap\_get\_rec.py$, respectively, mines the association patterns from the database and computes the recommendation procedure in the AP-phase. Lastly, $cfap\_get\_rec.py$ is the script responsible of the merging of the two CF and AP approaches. The $diversity\_calculation.py$ script analyses the target user history and, exploiting the function written in the $shannon.py$ file, calculates the Shannon Diversity Indexes of each user. 

\begin{figure}[h]
    \centering
    \includegraphics[width=6cm]{../Plots/filesystem.pdf}
    \caption{Project file-system}
    \label{fig:fs}
\end{figure}
