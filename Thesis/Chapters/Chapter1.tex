% Chapter 1

\chapter{Introduction} % Main chapter title

\label{Chapter1} % For referencing the chapter elsewhere, use \ref{Chapter1} 

%----------------------------------------------------------------------------------------

% Define some commands to keep the formatting separated from the content 
\newcommand{\keyword}[1]{\textbf{#1}}
\newcommand{\tabhead}[1]{\textbf{#1}}
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{\bfseries#1}}
\newcommand{\option}[1]{\texttt{\itshape#1}}

%----------------------------------------------------------------------------------------

%intro
Recommender Systems (RS) are compositions of software tools and machine learning techniques that provide the user with a valuable list of items or services he might prefer, or predict how much he might
prefer each of them. This process alleviates the task of finding more favourable items in the collection through encouraging the users to decide on suitable items \cite{RSsota}.

%definition
In general, RSs first take a set of input which could be user profiles, a set of item ratings, etc., identify similarities among the input, and use the found patterns in order to compute prediction calculation. Figure \ref{fig:RS} shows a schema of the recommendation process. Formally speaking, we can define the Recommender Problem as follows.

\theoremstyle{definition}
\begin{definition}{Recommender Problem} 
The Recommender Problem consists in estimating an utility function $f_u : U \times I \rightarrow R $ to predict how a user will like an item with $U$:= {set of all users}, $I$:= {set of all the recommendable items} and $R$:= {recommended items}. For each user $u \in U$, we want to choose the items $i \in I$ that maximize $f_u$.
\end{definition}

%implementation
Designing and developing RSs is a multi-disciplinary effort that involves results obtained in a number of various computer science fields such as Artificial Intelligence, Data Mining, Statistics and Human Computer Interaction. On the other side, this kind of research is being conducted with a strong emphasis on practice and commercial applications, since, aside from its theoretical contribution, is generally aimed at practically improving commercial platforms. 

%applications
Currently, those systems can now be found in many modern domains that expose the user to a huge collections of items. Some general classes of applications can be defined \cite{hb}:
\begin{itemize}
\item \textit{Entertainment.} Recommendations for movies, music, and IPTV.
\item \textit{Content.} Personalized newspapers, recommendation for documents, recommendations of Web pages, e-learning applications, and e-mail filters.
\item \textit{E-commerce.} Recommendations for consumers of products to buy such as books, cameras, PCs etc.
\item \textit{Services.} Recommendations of travel services, recommendation of experts for consultation, recommendation of houses to rent, or matchmaking services.
\end{itemize}
For example, the streaming service Netflix \cite{netflix} shows predicted ratings for every
displayed movie in order to help the user decide which movie to see. The online
book retailer Amazon \cite{amazon} provides average user ratings for displayed books, and a list
of other books that are bought by users who buy a specific book. Microsoft \cite{microsoft} provides
many free downloads for users, such as bug fixes, products and so forth. When a
user downloads some software, the system presents a list of additional items that are
downloaded together. All these systems are typically categorized as recommender
systems, even though they provide diverse services \cite{evalms}. 

Of course, the domain and particular problem strongly influence goals and used techniques. These aspects are relevant to different stages in the life cycle of a RS, namely, the design of the system, its implementation and its maintenance and enhancement during system operation.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{../Figures/RS_.pdf}
    \caption{The Recommendation Process}
    \label{fig:RS}
\end{figure}

%goals
Trying to define how a good recommendation should be, we can list some main aspects:
\begin{itemize}
\item \textit{Personalized.} It must be relevant to the user according to his/her tastes
\item \textit{Diverse.} It must represents all the possible interests of one user and it do not have to recommend items the user already knows or would have found anyway. It must be able to expand the user taste into neighbouring areas.
\end{itemize}

%tecniques
Almost all recommender systems fall into the following categories:
\begin{itemize}
\item \textit{Content-based.} Recommend items similar to those items the target user already liked in the past.
\item \textit{Collaborative-based.} Find users who share similar tastes with the target user and recommend articles they like.
\item \textit{Demographic-based.} Generate recommendations using user's attributes.
\end{itemize} 
More than that, there exist a lot of other minor approaches and often exploited to combine multiple recommendation techniques together to achieve a synergy between them in order to achieve better results. Those aspects will be better discussed in Chapter 2.

%motivations
RS are the focus of the common current interest and of this thesis because of the importance they represent nowadays for the business ecosystem. In the last years, the interest in recommender systems has incredibly increased and this kind of technologies have become one of the most powerful and popular tools in electronic commerce and have been successfully deployed in every commercial environment. Although many different approaches to recommender systems have been developed within the past few years, the interest in this area still remains high. An example of this fact is the important role they play in some of the most highly rated Internet sites:
\begin{itemize}
\item on \textit{Netflix} the 75\% of the movies watched are recommended
\item on \textit{GoogleNews} recommendations generate 38\% more click-throughs 
\item on \textit{Amazon} the 35\% sales comes from recommendations
\item on \textit{Choicestream} 28\% of the people would buy more music if they found what they liked
\item on \textit{Youtube} video recommendation accounts for about 30\% of overall views
\end{itemize}
Furthermore, the public interest on the argument is confirmed by the more and more dedicated conferences and workshops related to the field such as the ACM Recommender Systems (RecSys), launched in 2007 and now the premier annual event in recommender technology research and applications.  

%the movie domain
The concrete movies domain is a particular explored field. Internet TV is about choice: what to watch, when to watch, and where to watch, compared with linear broadcast and cable systems that offer whatever is now playing on perhaps 10 to 20 favourite channels. With more than 30,000 TV channels and 50,000 new movies a year, choosing what is worthy becomes a burning issue and users are surprisingly bad at choosing between many options, quickly getting overwhelmed and choosing “none of the above” or making poor choices. A consumer research suggests that a typical Netflix member loses interest after perhaps 60 to 90 seconds of choosing, having reviewed 10 to 20 titles (perhaps 3 in detail)
on one or two screens \cite{netflixrs}. The user either finds something of interest or the risk of the user abandoning our service increases substantially. The recommender problem is to make
sure that on those two screens each member in our diverse pool will find something
compelling to view, and will understand why it might be of interest. At the same time, a benefit of Internet TV is that it can carry videos from a broader catalogue appealing to a wide range of demographics and tastes, and including niche titles of interest only to relatively small groups of users. 
To confirm the importance of the issue, recently Netflix developed an algorithm of predicting users' movie ratings and in order to further improve it, provided a data set of 480,195 users, 17,770 movies, and 100 million ratings to the public and awarded a million dollar prize to the team that first succeeded in improving substantially the performance of its recommender system \cite{netflixrs}. 

%motivations
I initially started to work in this area of interest due to a collaboration with BAGMovies \cite{bagmovies}, a young start-up company providing an online recommender service created to help people decide what to watch, and share opinions about movies, series, shorts and documentaries. This experience convinced me that intelligent systems are surely an issue that deserves to be explored.

%algorithm
As said before, many media companies are now developing and deploying RSs as part of the services they provide to their subscribers. Despite this, the number of published techniques available in the literature is quite limited. Furthermore, while the majority of algorithms proposed in recommender systems literature have focused on improving recommendation accuracy, other important aspects of recommendation quality have often been overlooked and the ability for the recommendation techniques to improve both accuracy and diversity at the same time remains largely unexplored.

In this thesis, we focused on the combination of a pure Collaborative-Filtering approach and an Association Rules strategy, showing that the co-existence of both the techniques is quite appropriate for this task and they can achieve better performance together. Furthermore, we completed the algorithm proposing a simple ranking technique able to increase the diversity in the final recommended set of items, taking into account the propensity of the single user for the novelty of the most significant movie attributes. Finally we provided a comprehensive empirical evaluation of the proposed approaches, where we tested them exploring a variety of different settings.
