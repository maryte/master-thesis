\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Abstract}{ii}{section*.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgements}{iii}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}State of the Art}{5}{chapter.11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Recommender System Functions}{5}{section.12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}The service provider's role}{5}{subsection.13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}The user's role}{6}{subsection.14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Data and knowledge sources}{8}{section.15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Items}{9}{subsection.16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Users}{9}{subsection.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.3}Transaction}{10}{subsection.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Explicit}{10}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Implicit}{11}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Recommendation Techniques}{11}{section.21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Content-based approach}{12}{subsection.23}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Collaborative Filtering approach}{14}{subsection.24}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Euclidian Distance}{15}{section*.25}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Pearson Correlation Coefficient}{16}{section*.27}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Demographic Filtering}{16}{subsection.29}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}Other Approaches}{17}{subsection.30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Association Rules}{17}{section*.31}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Knowledge-based}{18}{section*.32}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Constraint-based}{19}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Community-based}{19}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Clustering Users and Items}{19}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.5}Hybrid recommender systems}{20}{subsection.36}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Evaluation Protocols}{21}{section.37}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Offline experiments}{22}{subsection.38}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}User studies}{22}{subsection.39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.3}Online evaluation}{23}{subsection.40}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Challenges and Issues}{24}{section.41}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.1}Cold-start}{24}{subsection.42}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.2}Trust}{24}{subsection.43}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.3}Scalability}{24}{subsection.44}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.4}Sparsity}{24}{subsection.45}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.5}Privacy}{25}{subsection.46}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.6}Similarity vs. Diversity}{25}{subsection.47}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Real Recommendation Systems}{26}{section.48}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.1}Google}{26}{subsection.49}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.2}Youtube}{27}{subsection.50}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.3}Amazon}{27}{subsection.51}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.4}Netflix}{28}{subsection.52}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}System Overview: a Hybrid Recommender System}{30}{chapter.53}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Problem Description}{30}{section.54}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Design and Methodology}{30}{section.55}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Collaborative Filtering}{31}{subsection.57}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Association Rules}{33}{subsection.59}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Mixed strategy}{34}{subsection.61}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Diversification}{36}{subsection.63}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Implementation}{38}{section.65}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Experimental Tuning and Evaluation}{40}{chapter.67}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Datasets}{41}{section.68}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Transactions}{41}{subsection.69}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Movies}{41}{subsection.70}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Evaluation}{41}{section.71}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Performance Measures}{41}{subsection.72}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Experimental Results}{42}{subsection.73}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Number of closer users tuning}{43}{section*.74}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Minimum Support and Confidence tuning}{44}{section*.76}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Mixed strategy evaluation}{45}{section*.79}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusions}{46}{chapter.81}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Future works}{47}{section.82}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Address the cold-start problem}{47}{subsection.83}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Other movie features}{47}{subsection.84}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Further datasets}{47}{subsection.85}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}Further testing procedure}{48}{subsection.86}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliography}{49}{appendix*.87}
